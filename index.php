<?php
    include ('animal.php');
    include ('Ape.php');
    include ('Frog.php');

    //release 0
    $sheep = new Animal("shaun");

    echo $sheep->get_name().'<br>'; // "shaun"
    echo $sheep->get_legs().'<br>'; // 2
    echo $sheep->get_cold_blooded().'<br>'; // false

    //release 1
    echo '<br>';
    $sungokong = new Ape("kera sakti");
    echo $sungokong->yell().'<br>'; // "Auooo"

    $kodok = new Frog("buduk");
    echo $kodok->jump().'<br>'; // "hop hop"
?>