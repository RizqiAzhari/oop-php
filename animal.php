<?php

    class Animal {
        private $legs;
        private $cold_blooded;
        private $name;
        public function __construct($name, $legs = 2, $cold_blooded = "false"){
            $this->name = $name;
            $this->legs = $legs;
            $this->cold_blooded = $cold_blooded;
        }

        public function get_name(){
            return $this->name;
        }

        public function get_legs(){
            return $this->legs;
        }

        public function get_cold_blooded(){
            return $this->cold_blooded;
        }
    }
?>