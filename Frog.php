<?php

    class Frog extends Animal {
        public function __construct($name, $legs = 4){
            parent::__construct($name, $legs);
        }

        public function jump(){
            return 'hop hop';
        }

    }
?>